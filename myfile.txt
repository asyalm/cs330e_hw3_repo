In the darkness, he wept. Silently, for even if another soul was near, he could not be helped. Gone was the chance for redemption.
For change. For the feelings of warmth on his skin from the morning sun. 
Now, he was alone in the darkness.
He wept.
